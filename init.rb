# Define the github handler
require 'autoproj/git_server_configuration'
require 'autoproj/repository_managers/apt'

# Comment this line if you want Autoproj to import the overall shell
# environment. The default is to reset all build-related environment variables,
# forcing the build configuration to set them explicitly, to improve
# repeatability
Autoproj.isolate_environment

# Display CMake/GCC build messages
Autobuild::CMake.show_make_messages = true

Autoproj.git_server_configuration 'GSCAR', 'gitlab.com',
    default: 'http,ssh', disabled_methods: 'git',
    http_url: 'https://gitlab.com/gscar-coppe-ufrj',
    ssh_url: 'git@gitlab.com:gscar-coppe-ufrj'

Autoproj.env_add_path "LD_LIBRARY_PATH", File.join(Autoproj.root_dir, "install/lib")

module Autobuild
    def self.scripts(spec, &proc)
        ScriptPackage.new(spec, &proc)
    end

    class ScriptPackage < ImporterPackage
        def prepare
            super
            Autobuild.env.add_path 'PATH', File.join(srcdir)
            files_auto_complete = Dir[File.join(srcdir, "completion/*")]
            files_auto_complete.each do |file|
                Autobuild.env.source_after file
            end
        end
    end
end

# Defines a script package in the installation
def script_package(name, workspace: Autoproj.workspace, &block)
    package_common(:scripts, name, workspace: workspace, &block)
end

ros_key = "C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654"
has_ros_key = !`APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=" " bash -c "apt-key list | sed -r 's/ //g' | grep #{ros_key}"`.empty?
if !has_ros_key
    apt_ros = Autoproj::RepositoryManagers::APT.new(Autoproj.workspace)
    apt_ros.add_apt_key(ros_key, "hkp://keyserver.ubuntu.com:80")
end
has_ros = !`apt-cache search ros-dashing`.empty?
if !has_ros
    has_ros = !`apt-cache policy | grep ros2`.empty?
    if !has_ros
        `sudo cp #{Autoproj.workspace.config_dir}/sources.list.d/ros2-latest.list /etc/apt/sources.list.d`
        `sudo apt-get update`
    end
end
