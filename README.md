# Description

This is the main build configuration of an autoproj installation

See http://rock-robotics.org/stable/documentation/autoproj for detailed information about autoproj.

Since this build configuration is on gitlab at
https://gitlab.com/gscar-coppe-ufrj/ariel/buildconf, a workspace based on this build
configuration can be bootstrapped using:

```
sudo apt install ruby-dev cmake libffi-dev libffi6
wget http://rock-robotics.org/autoproj_bootstrap
ruby autoproj_bootstrap git https://gitlab.com/gscar-coppe-ufrj/ariel/buildconf
```

Once bootstrapped, the main build configuration is checked out in the
workspace's `autoproj/` folder.

# First time configuration

To pull from all remotes and synchronize your workspace for the first time, run
`aup`. This command should be called every time you need to update packages on
your workspace.

After running `aup`, you should see the `scripts/ariel` directory. This package
hosts useful scripts for the Ariel project. Since this is your first time
configuring the project, you should call `ariel-configure` and follow the
prompts. You may call this script again at any time. Have a look at the
[ariel/scripts](https://gitlab.com/gscar-coppe-ufrj/ariel/scripts) repository
for more information on this package (that is, if someone has already bothered
to write a readme there).

# Day-to-day workflow

`aup` updates the current package (or, if within a directory, the packages
under that directory). Use `--all` to update the whole workspace.

`amake` build the current package (or, if within a directory, the packages
under that directory). Use `--all` to build the whole workspace.

`ariel make` always builds the whole workspace, but has options to build ROS
packages, which are not built when calling `amake`. This command defaults to
building all pure CMake and ROS packages, but one may use the `ariel-make --only-lib`
and `ariel-make --only-ros` varieties to choose between one of the two. Run
`ariel-make --help` to see the options.

`acd` is a quick replacement for the usual `cd` command that can be used to
navigate through your Autoproj workspace. Try `acd` with no options to go to
the workspace root directory or `acd package/name` to go straight to a package.
It is also possible to abbreviate the former command like `acd p/n` or `acd pack/na`
if there is only on match for this abbreviations on your workspace.

`autoproj status` will show the difference between the local workspace and
the remote repositories

`autoproj show PACKAGE` will display the configuration of said package, which
includes whether that package is selected in the current workspace ("a.k.a.
will be updated and built") and the import information (where the package
will be downloaded from)

# Overriding default repository settings

It is possible to override the default configuration for each remote repository
that is probed when calling `aup`. For that, create (or edit) a file in the
`autoproj/override.d` directory following the regular expression
"[0-9][0-9]-\w*.yml", e.g. 01-git.yml and 03-wip.yml are valid, but 2-git.yml
and 03-wip are not (these last two files would actually be understood by
Autoproj, but we advise against using these names). For instance, to change the
branch of control/scar and both the branch and the remote of scripts/ariel,
create (edit) 01-git.yml like this:

```yml
- control/scar: # package name and also the location under the root directory
    branch: develop/trajectory/class-trajectory

- scripts/ariel: # package name and also the location under the root directory
    branch: some/other/branch
    github: profile/project # for now, valid remotes are github, gitlab and gscar
```

# Additional information

## Autoproj configuration structure

The main build configuration tells autoproj about:

- which extra configurations should be imported ("package sets") in the
  `package_sets` section of the `manifest`. Once loaded, these package
  sets are made available in the `remotes/` subfolder of this configuration.
- which packages / package sets should be built in the `layout` section of
  the `manifest`
- which local overrides should be applied on package import (e.g. allowing
  to change a package's import branch or URL) in `.yml` files located in
  the `overrides.d` folder.

Overall, autoproj does the following with its configuration:

- load package description by (1) importing the package sets listed in
  `manifest` and (2) loading the `.autobuild` files in the imported package sets.
  Imported package sets are made available in the `remotes/` folder of this
  directory.
- resolve the import information (which can be inspected with `autoproj show`)

## Using extra RubyGems in an autoproj workspace

One can add new gems to a workspace, without passing through autoproj's osdeps
system, by adding new `.gemfile` files to the workspace's
`autoproj/overrides.d/` folder. These files must follow
[Bundler](http://bundler.io) gemfile format

